![icon](http://i.imgur.com/FgygHHy.png)

Pirates Online Rewritten Documentation
========================================

Overview
--------

*Pirates Online Rewritten*, aka POR, is an ongoing project to recreate *Disney's Pirates of the Caribbean Online*. As the development of POR continues, documentation will be written. Any documenation that is safe for public view will be put in this repository.  Any sensitive information, such as how to compile the game, will not.

Documenation
------------
This readme will serve as an index for all documenation on this repository. POR is still very early in development, most documenation that would be posted hasn't been written yet. Over time this repository will gain size.

### ~ Building Astron

[Building Astron on Windows](https://bitbucket.org/piratesonlinerewritten/documentation/src/master/building/BuildingAstron-Windows.md)